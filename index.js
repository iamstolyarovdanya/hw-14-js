const body = document.body
const p1 = document.querySelector(`.text1`);
const p2 = document.querySelector(`.text2`);
const header = document.querySelector(`.header_menu `);
const aside = document.querySelector(`.aside_menu `);
const btn = document.querySelector(`.btn`);
btn.addEventListener(`click`, (event) => {
  header.classList.toggle(`active`);
  aside.classList.toggle(`active`);
  p1.classList.toggle(`active_color`);
  p2.classList.toggle(`active_color`);
  localValue()
})
function localValue  ()  {
  const asidee1 = {
    btnActive: header.classList.contains('active'),
    aside: aside.classList.contains(`active`),
    p1: p1.classList.contains(`active_color`),
    p2: p2.classList.contains(`active_color`),
  };
  localStorage.setItem('appState', JSON.stringify(asidee1)) 
};
function take ()  {
  const takeValue = localStorage.getItem('appState');
  if (takeValue) {
    const open = JSON.parse(takeValue);
    if (open.btnActive && open.aside && open.p1 && open.p2) {
      header.classList.add('active');
      aside.classList.add(`active`);
      p1.classList.add(`active_color`);
      p2.classList.add(`active_color`);
    }
  }
};
take()